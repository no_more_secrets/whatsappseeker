# WhatsAppSeeker

Tool to search the WhatsApp database file (iOS) for user (owner) posts of media 
files and who actually recieved/viewed them from the view of the writer/poster!
Prerequisites: the DB-File (ChatStorage.sqlite), and the filename of the 
(incriminated) mediafile/document.

WhatsAppSeeker dient der schnellen Extraktion der Informationen zu 
Verbreitungshandlungen (Forensik) in WhatsApp-Chats.
Die Suche umfasst alle Chats des Users in der gewählten Datenbank.

Die derzeit gängigen Versionen von UFED und XRY stellen entweder nur ungenügend 
aussagekräftige Informationen (XRY) bei der Auswertung der IOS-Datenbank dar
oder gar keine (UFED).
