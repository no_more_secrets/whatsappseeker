import sqlite3
import time
from PyQt5.QtCore import Qt
from PyQt5.uic import *
import sys
from os import path, environ
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog, QMessageBox, QTreeWidgetItem, QTreeWidgetItemIterator


__version__ = '$1.0.2019.05.08.2000 indev python3.7-pyqt5.12$'
__date__ = '25.01.2019'
__author__ = 'Unkraut77 (https://gitlab.com/no_more_secrets/)'
__release__ = 'master'


ui_path = path.dirname(path.abspath(__file__))

sqlQuerryReceivers = """Select ZWAPROFILEPUSHNAME.ZJID As 'Recipient',
                    ZWAPROFILEPUSHNAME.ZPUSHNAME As 'Nickname'
From ZWAPROFILEPUSHNAME"""

sqlQuerryParticipants2 = """SELECT ZWAMESSAGEINFO.ZRECEIPTINFO, 
                    ZWAMESSAGE.ZCHATSESSION As 'Chatsession',
                    ZWAMESSAGE.Z_PK As 'Chatmessage',
                    ZWACHATSESSION.ZPARTNERNAME As 'ChatPartner',
                    ZWAMESSAGE.ZTOJID As 'Partnername',
                    ZWAMESSAGE.ZMESSAGEDATE As 'MessageDate',
                    ZWAMESSAGE.ZSENTDATE As 'SentDate'
    FROM ZWACHATSESSION 
        INNER JOIN ZWAMEDIAITEM ON ZWACHATSESSION.Z_PK = ZWAMESSAGE.ZCHATSESSION
        INNER JOIN ZWAMESSAGE ON ZWAMEDIAITEM.Z_PK = ZWAMESSAGE.ZMEDIAITEM
        INNER JOIN ZWAMESSAGEINFO ON ZWAMESSAGE.Z_PK = ZWAMESSAGEINFO.ZMESSAGE
    WHERE 
        ZWAMEDIAITEM.ZMEDIALOCALPATH GLOB '*{}'"""

number = []
strFileSectionName = 'Datei'
strChatsessionSectionName = 'Chatsession'
strMessageSectionName = 'Message'
strGruppenbesitzerSectionName = 'Gruppenbesitzer'
strDatumSectionName = 'Datum'
strEmpfaengerSectionName = 'Empfänger'
strBroadcastSuffix = '@broadcast'
strGroupSuffix = '@g.us'
strUserSuffix = '@s.whatsapp.net'
strDateTimeFormat = '%d-%m-%Y %H:%M:%S'
strUIFile = 'frm_WASeeker2.ui'


class uicWAS(QMainWindow):

    sInfoBoxText = '- WhatsAppSeeker dient der schnellen Extraktion der Informationen zu Verbreitungshandlungen in WhatsApp-Chats.\n\n' \
                  '- Die Suche umfasst alle Chats in der gewählten Datenbank.\n\n- Bei Fragen und Anregungen:\n\t' \
                  'Helmut Schmidt, KD BB/K5, schmi171@polizei.bwl.de'
    sInfoBoxTitle = 'WhatsAppSeeker Info'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.ui = loadUi(path.join(ui_path, strUIFile), self)
        self.strDBFile = ''
        self.dictRecipients = {}
        self.ui.setupUI()

    def setupUI(self):
        self.pbtnDBOpen.clicked.connect(self.pbtnDBOpen_clicked)
        self.pbtnStartSearch.clicked.connect(self.pbtnStartSearch_clicked)
        self.actionInfo.triggered.connect(self.defMenuInfoBox)
        self.actionHilfe.triggered.connect(self.defMenuHilfe)

    def pbtnDBOpen_clicked(self):
        pathDesktop = path.join(environ['USERPROFILE'], 'Desktop')
        try:
            options = QFileDialog.Options()
            # options |= QFileDialog.DontUseNativeDialog
            self.strDBFile, _ = QFileDialog.getOpenFileName(self, "Auswahl der Datenbank-Datei", pathDesktop, options=options)
            if not self.strDBFile or self.strDBFile[0] == ".":
                return
            self.label_Ueberschrift.setText('Datenbank: ' + self.strDBFile)
        except OSError as synterr:
            # print(synterr)
            self.statusbar.showMessage(synterr, 4000)
            pass

    def pbtnStartSearch_clicked(self):
        if self.strDBFile == '':
            self.pbtnDBOpen_clicked()
            if self.strDBFile == '':
                return
        try:
            connection = sqlite3.connect(self.strDBFile)
        except:
            # pass
            self.statusbar.showMessage('Keine DB ausgewählt', 4000)
            return

        cursor = connection.cursor()
        dictData = {}
        self.ui.tedDump.clear()
        self.defClearTreeWidget()

        if self.ledMediaName.text() == '':
            self.statusbar.showMessage('Kein Suchbegriff angegeben', 4000)
            return
        cursor.execute(sqlQuerryReceivers)
        self.dictRecipients = dict(cursor.fetchall())

        cursor.execute(sqlQuerryParticipants2.format(self.ledMediaName.text()))
        listResult2 = cursor.fetchall()
        defAusgabe2(listResult2, self.tedDump, dictData, self.ledMediaName.text(), self.dictRecipients)

        root = QTreeWidgetItem(self.ui.treeRecipients, dictData)
        fill_item(root, dictData['Datei'])
        self.ui.treeRecipients.expandToDepth(7)

        cursor.close()
        connection.close()

    def defClearTreeWidget(self):
        iterator = QTreeWidgetItemIterator(self.ui.treeRecipients, QTreeWidgetItemIterator.All)
        while iterator.value():
            iterator.value().takeChildren()
            iterator += 1
        i = self.ui.treeRecipients.topLevelItemCount()
        while i > -1:
            self.ui.treeRecipients.takeTopLevelItem(i)
            i -= 1

    def defMenuInfoBox(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(self.sInfoBoxText + '\n\n-Version: ' + __version__.replace('$', ''))
        msg.setWindowTitle(self.sInfoBoxTitle)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def defMenuHilfe(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setTextFormat(Qt.RichText)
        msg.setText('<font size="10">Echt jetzt?</font>')
        msg.setWindowTitle('WhatsAppSeeker Hilfe')
        msg.setStandardButtons(QMessageBox.Ok)
        # msg.setFixedWidth(400)
        layout = msg.layout()
        item = layout.itemAtPosition(0, 2)
        item.widget().setFixedWidth(200)
        msg.exec_()


def fill_item(item, value):
    def new_item(parent, strText, objValue=None):
        child = QTreeWidgetItem([strText])
        fill_item(child, objValue)
        if type(parent) == QTreeWidgetItem:
            parent.addChild(child)
        else:
            return
        child.setExpanded(True)
    if value is None:
        return
    elif isinstance(value, dict):
        for key, val in sorted(value.items()):
            new_item(item, str(key), val)
    elif isinstance(value, (list, tuple)):
        for val in value:
            text = (str(val)) if not isinstance(val, (dict, list, tuple)) else ''.format(type(val).__name__)
            new_item(item, text, val)
    else:
        new_item(item, str(value))


def defAusgabe2(listResult, widgetOutput, dictData, strFile, dRecipients):
    dictData[strFileSectionName] = {}          # Info: sollten je mehrere Dateinamen gesucht werden, muss diese Zeile außerhalb der Iterierung bleiben!
    dAllChatTemp = {}
    dAllChatTemp[strChatsessionSectionName] = {}
    dAllMedia = {}
    dAllChat = {}

    if listResult:
        for listElement in listResult:              # INFO: Jeder Abfragetreffer wird bearbeitet: Treffer = Nachricht
            widgetOutput.insertPlainText('\nAbfrageergebnis: {}\n'.format(', '.join(str(x) for x in listElement)))
            c = (listElement[0])
            dEmpf = {}
            dMsg = {}       # Info: neue Instanz, kein deepcopy notwendig
            dAllMsg = {}
            dAllMsgTemp = {}
            dChat = {}

            if listElement[4][-5:] == strGroupSuffix or strBroadcastSuffix in listElement[4]:
                widgetOutput.insertPlainText('  Gruppenchat-Besitzer: {} - Gruppenname: {}\n'.format(listElement[4], listElement[3]))
                widgetOutput.insertPlainText('  Folgende Gruppenmitglieder der Chatsession {0} haben das Bild {2} mit der Messagenummer {1} am {3} erhalten:\n'
                                             .format(listElement[1], listElement[2], strFile, time.strftime(strDateTimeFormat, time.localtime(listElement[5] + 978307200))))
                dictData.setdefault('Media-Datei', {})['Chatsession'] = listElement[1]
            else:
                widgetOutput.insertPlainText('  Folgender Empfänger einer Direktnachricht der Chatsession {0} hat das Bild {2} mit der Messagenummer {1} am {3} erhalten:\n'
                                             .format(listElement[1], listElement[2], strFile, time.strftime(strDateTimeFormat, time.localtime(listElement[5] + 978307200))))

            for a in c.decode('latin-1')[3:].split('\n'):       # Info hier wird nur c = (listElement[0]) decodiert
                # print(a)
                for i in range(len(a)):                     # Info hier wird ein Substring 'a' von c.split('\n') decodiert
                    # print(a[i])
                    try:
                        temp = int(a[i])        # Info: wenn der aktuelle char 'a[i]' keine Zahl darstellt
                    except:
                        if a[i] == '\n':        # Info wenn der aktuelle char kein Steuerzeichen (Zeilenumbruch) ist
                            temp = '_'
                        else:
                            temp = 0.5          # Info: temp wird zu einem double...
                        pass
                    if isinstance(temp, (int, str)):    # Info: ...und fällt bei der int-Prüfung durch und wird nicht dem Puffer 'number' hinzugefügt
                        number.append(temp)             # Info: ...oder es ist ein erlaubtes Zeichen und wird hinzugefügt
                if len(number) is not 0:
                    strKey = ''.join(str(u) for u in number) + strUserSuffix
                    strValue = dRecipients[strKey]
                    strOutput = '  ' + strKey + ': {}\n'.format(strValue)
                    widgetOutput.insertPlainText(strOutput)
                    dEmpf[strKey] = strValue
                number.clear()
            dMsg[listElement[2]] = {strDatumSectionName: time.strftime(strDateTimeFormat, time.localtime(listElement[5] + 978307200)),
                                    strEmpfaengerSectionName: dEmpf, strGruppenbesitzerSectionName: {listElement[4]: listElement[3]}}
            if dMsg[listElement[2]].get(strGruppenbesitzerSectionName) == '':
                del dMsg[listElement[2]][strGruppenbesitzerSectionName]
            dAllMsg.update(dMsg)        #Info: Alle MsgIDs werden unter 'Message' zusammengetragen
            dAllMsgTemp = {strMessageSectionName: dAllMsg}
            dChat = {listElement[1]: dAllMsgTemp}
            dAllChat.update(dChat)
            dAllChatTemp[strChatsessionSectionName].update(dAllChat)
            # print(widgetOutput.toPlainText())
    else:
        widgetOutput.insertPlainText('\nAbfrageergebnis: {}\n'.format('keine Treffer'))
        dAllChatTemp = 'keine Treffer'
    dMedia = {strFile: dAllChatTemp}
    dAllMedia.update(dMedia)

    dictData[strFileSectionName].update(dAllMedia)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    uiHauptfenster = uicWAS()
    uiHauptfenster.show()
    sys.exit(app.exec_())

